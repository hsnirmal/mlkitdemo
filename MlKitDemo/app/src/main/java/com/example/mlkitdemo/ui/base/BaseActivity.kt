package com.example.mlkitdemo.ui.base

import android.content.Context
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.mlkitdemo.R
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel<BaseNavigator>> : AppCompatActivity(), BaseNavigator{
    private val progressDialog = CustomProgressBar()

    var mViewDataBinding: T? = null
        private set

    private var mViewModel: V? = null

    val activityNavController: NavController? by lazy {
        when {
            isNavigationSupported() -> {
                findNavController(R.id.nav_host_fragment)
            }
            else -> {
                null
            }
        }
    }

    /**
     * Flag to include and initialize navigation controller
     *
     */
    abstract fun isNavigationSupported(): Boolean

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @IdRes
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
    }

    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = mViewModel ?: getViewModel()
        mViewDataBinding!!.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding!!.executePendingBindings()
    }

    override fun finish() {
        super.finish()
        // To make activity support navigation graph animation
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    // Base Navigator methods
    override fun showLoading() {
        progressDialog.show(this)
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }

    override fun showMessage(message: String?) {
        showSnackBar(mViewDataBinding!!.root, message, Snackbar.LENGTH_LONG, false)
    }

    override fun showError(message: String?) {
        showSnackBar(mViewDataBinding!!.root, message, Snackbar.LENGTH_LONG, true)
    }

    override fun showError(messageId: Int) {
        showSnackBar(mViewDataBinding!!.root, getString(messageId), Snackbar.LENGTH_LONG, true)
    }

     override fun showMessage(messageId: Int) {
        showSnackBar(mViewDataBinding!!.root, getString(messageId), Snackbar.LENGTH_LONG, false)
    }

    private fun showSnackBar(view: View, message: String?, time: Int, isTypeError: Boolean) {
        var snackMessage = message
        if (snackMessage == null) {
            snackMessage = ""
        }

        val snackbar = Snackbar.make(view, snackMessage, time)
        val snackBarView = snackbar.view
        val snackTextView = snackBarView.findViewById<View>(R.id.snackbar_text) as TextView
        snackTextView.maxLines = 4
        /*  if (isTypeError) {
              snackBarView.setBackgroundColor(Color.parseColor("#E41200"))
          } else {
              snackBarView.setBackgroundColor(Color.parseColor("#34A853"))
          }*/
        snackbar.show()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}