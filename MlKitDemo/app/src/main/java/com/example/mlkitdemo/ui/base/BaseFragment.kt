package com.example.mlkitdemo.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.mlkitdemo.R

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel<BaseNavigator>> :
    Fragment() , BaseNavigator{

    lateinit var mRootView: View
    private lateinit var mViewDataBinding: T
    private lateinit var mViewModel: V

    // Initialize navigation controller
    val fragmentNavController: NavController by lazy { findNavController() }
    val activityNavController: NavController? by lazy { activity?.findNavController(R.id.nav_host_fragment) }

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    protected fun getViewDataBinding(): T = mViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        // Log fragment lifecycle log in sentry
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
    }

    override fun onDestroy() {
        super.onDestroy()
        // Log fragment lifecycle log in sentry
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(
            inflater,
            getLayoutId(),
            container,
            false,
        )
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding.executePendingBindings()

        mViewDataBinding.lifecycleOwner = viewLifecycleOwner
        mRootView = mViewDataBinding.root
        onCreateView(mRootView, savedInstanceState)
        return mRootView
    }

    open fun onCreateView(view: View, savedInstanceState: Bundle?) {
    }

    override fun onResume() {
        super.onResume()
    }

    protected open fun getBaseActivity(): BaseActivity<*, *> =
        requireActivity() as BaseActivity<*, *>


    // Base Navigator methods
    override fun showLoading() {
        getBaseActivity().showLoading()
    }

    override fun hideLoading() {
        getBaseActivity().hideLoading()
    }

    override fun showMessage(message: String?) {
        getBaseActivity().showMessage(message)
    }

    override fun showMessage(messageId: Int) {
        getBaseActivity().showMessage(messageId)
    }

    override fun showError(message: String?) {
        getBaseActivity().showError(message)
    }

    override fun showError(messageId: Int) {
        getBaseActivity().showError(messageId)
    }
}