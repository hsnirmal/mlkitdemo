package com.example.mlkitdemo.ui.base

import androidx.annotation.StringRes

interface BaseNavigator {
    fun handleError(throwable: Throwable?)
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String?)
    fun showMessage(@StringRes messageId: Int)
    fun showError(message: String?)
    fun showError(@StringRes messageId: Int)
}
