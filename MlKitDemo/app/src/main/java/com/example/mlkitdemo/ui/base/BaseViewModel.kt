package com.example.mlkitdemo.ui.base

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

abstract class BaseViewModel<N : BaseNavigator>() : ViewModel() {
    val isLoading = ObservableBoolean(false)
    val isEmpty = ObservableBoolean(false)
    val isError = ObservableBoolean(false)
    val errorText = ObservableField<String>("Error while fetching data")
    val compositeDisposable = CompositeDisposable()

    lateinit var mNavigator: WeakReference<N>

    fun getNavigator(): N? {
        return mNavigator.get()
    }

    fun setNavigator(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}

