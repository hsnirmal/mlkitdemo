package com.example.mlkitdemo.ui.base

import android.app.Dialog
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.core.content.res.ResourcesCompat
import com.example.mlkitdemo.R
import kotlinx.android.synthetic.main.layout_progress_dialog.view.*

class CustomProgressBar {
    private var dialog: Dialog? = null

    fun show(context: Context): Dialog {
        return show(context, null)
    }

    fun show(context: Context, title: CharSequence?): Dialog {
        if (isShowing()) {
            dismiss()
        }

        val inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflator.inflate(R.layout.layout_progress_dialog, null)
        if (title != null) {
            view.cp_title.text = title
        }

        view.cp_bg_view.setBackgroundColor(Color.parseColor("#60000000")) //Background Color
        view.cp_cardview.setCardBackgroundColor(Color.parseColor("#50ffffff")) //Box Color

        setColorFilter(
            view.cp_pbar.indeterminateDrawable,
            ResourcesCompat.getColor(context.resources, R.color.colorPrimary, null)
        ) //Progress Bar Color

        view.cp_title.setTextColor(Color.WHITE) //Text Color
        dialog = Dialog(context, R.style.CustomProgressBarTheme)
        dialog?.setContentView(view)
        //Set the dialog to not focusable (makes navigation ignore us adding the window)
        dialog?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        )
        dialog?.show()

        return dialog!!
    }

    fun isShowing() = dialog?.isShowing ?: false

    fun dismiss() {
        dialog?.dismiss()
    }

    private fun setColorFilter(@NonNull drawable: Drawable, color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
        } else {
            @Suppress("DEPRECATION")
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }
}