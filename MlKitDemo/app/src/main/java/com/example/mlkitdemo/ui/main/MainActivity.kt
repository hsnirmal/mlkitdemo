package com.example.mlkitdemo.ui.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.mlkitdemo.BR
import com.example.mlkitdemo.R
import com.example.mlkitdemo.databinding.MainActivityBinding
import com.example.mlkitdemo.ui.base.BaseActivity

class MainActivity : BaseActivity<MainActivityBinding, MainActivityViewModel>() {

    override fun isNavigationSupported(): Boolean = true

    override fun getBindingVariable(): Int = BR.mainActivityViewModel

    override fun getLayoutId(): Int = R.layout.main_activity

    override fun getViewModel(): MainActivityViewModel {
        return ViewModelProvider(this).get(MainActivityViewModel::class.java)
    }

    override fun handleError(throwable: Throwable?) {
        TODO("Not yet implemented")
    }

    override fun onBackPressed() {
        activityNavController?.popBackStack()?.not()?.let {
            if (it)
                finish()
        }
    }
}