package com.example.mlkitdemo.ui.main

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.mlkitdemo.BR
import com.example.mlkitdemo.R
import com.example.mlkitdemo.databinding.MainFragmentBinding
import com.example.mlkitdemo.ui.base.BaseFragment
import iam.thevoid.mediapicker.rxmediapicker.Purpose
import iam.thevoid.mediapicker.rxmediapicker.RxMediaPicker

class MainFragment : BaseFragment<MainFragmentBinding, MainViewModel>() {

    override fun getBindingVariable(): Int {
        return BR.mainFragmentViewModel
    }

    override fun getLayoutId(): Int = R.layout.main_fragment

    override fun getViewModel(): MainViewModel {
        return ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewDataBinding().cameraCaptureButton.setOnClickListener {
            displayImagePicker()
        }
    }

    fun displayImagePicker() {
        val subscribe = RxMediaPicker.builder(requireContext())
            .pick(Purpose.Pick.IMAGE)
            .take(Purpose.Take.PHOTO)
            .build()
            .subscribe({ fileUri: Uri? ->
                fileUri?.path?.let {
                    getViewModel().imageUriLiveData.value = it
                }
            }, {
                it.printStackTrace()
            })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getViewModel().setNavigator(this)
        getViewModel().imageUriLiveData.observe(viewLifecycleOwner, {
            val action = MainFragmentDirections.actionMainFragmentToTextBlockFragment(it)
            fragmentNavController.navigate(action)
        })
    }

    override fun handleError(throwable: Throwable?) {
        TODO("Not yet implemented")
    }
}