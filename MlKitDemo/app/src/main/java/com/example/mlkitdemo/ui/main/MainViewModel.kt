package com.example.mlkitdemo.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mlkitdemo.ui.base.BaseNavigator
import com.example.mlkitdemo.ui.base.BaseViewModel

class MainViewModel : BaseViewModel<BaseNavigator>() {
    var imageUriLiveData = MutableLiveData<String>()
}