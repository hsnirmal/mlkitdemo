package com.example.mlkitdemo.ui.textblocks

import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mlkitdemo.BR
import com.example.mlkitdemo.R
import com.example.mlkitdemo.databinding.TextBlockFragmentBinding
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.example.mlkitdemo.ui.base.BaseFragment
import java.io.File

class TextBlockFragment : BaseFragment<TextBlockFragmentBinding, TextBlockViewModel>() {

    private lateinit var textBlocksAdapter: TextBlocksAdapter
    val args : TextBlockFragmentArgs by navArgs()

    override fun getBindingVariable(): Int {
        return BR.textBlockViewModel
    }

    override fun getLayoutId(): Int = R.layout.text_block_fragment

    override fun getViewModel(): TextBlockViewModel {
        return ViewModelProvider(this).get(TextBlockViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //init adapter
        textBlocksAdapter = TextBlocksAdapter(viewLifecycleOwner = viewLifecycleOwner,
        onClickCallback = {_,text->
            //handle click here
        })

        with(getViewDataBinding().rvTextBlocks) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = textBlocksAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getViewModel().setNavigator(this)
        getViewModel().textBoxLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            if(it.isNotEmpty())
                textBlocksAdapter.submitList(it)
            else
                showMessage("No data found")
        })

        context?.let { context ->
            val filePath = args.FILEPATH
            val savedUri = Uri.fromFile(File(filePath))
            val   image = FirebaseVisionImage.fromFilePath(context, savedUri)
            getViewModel().recognizeText(image)
        }
    }

    override fun handleError(throwable: Throwable?) {

    }
}