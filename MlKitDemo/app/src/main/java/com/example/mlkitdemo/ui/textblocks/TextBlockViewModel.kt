package com.example.mlkitdemo.ui.textblocks

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mlkitdemo.ui.base.BaseNavigator
import com.example.mlkitdemo.ui.base.BaseViewModel
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class TextBlockViewModel : BaseViewModel<BaseNavigator>() {

    var textBoxLiveData = MutableLiveData<List<String>>()

    fun recognizeText(image: FirebaseVisionImage) {
        getNavigator()?.showLoading()
        viewModelScope.launch (Dispatchers.IO){
            analyseImage(image)
        }
    }

    private suspend fun analyseImage(image: FirebaseVisionImage){
        val listOfTextBlocks = ArrayList<String>()
        try {
            val detector = FirebaseVision.getInstance()
                .onDeviceTextRecognizer
            val result = detector.processImage(image)
                .addOnSuccessListener { firebaseVisionText ->
                    for (block in firebaseVisionText.textBlocks) {
                        val blockText = block.text
                        Log.d("TEXT_RESULT_BLOCK", blockText);
                        listOfTextBlocks.add(blockText)
                    }
                    textBoxLiveData.value = listOfTextBlocks
                }
                .addOnFailureListener { e ->
                    Log.d("TEXT_RESULT", "" + e.message);
                }
                .addOnCanceledListener {
                    Log.d("TEXT_RESULT", "Canceled" );
                }
        } catch (e: Exception) {
            Log.d("TEXT_RESULT", "" + e.message);
            e.printStackTrace()
        }
    }
}