package com.example.mlkitdemo.ui.textblocks

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import com.example.mlkitdemo.R
import com.example.mlkitdemo.databinding.ListItemTextBlockBinding
import com.example.mlkitdemo.ui.base.DataBoundListAdapter
import com.example.mlkitdemo.ui.base.DataBoundViewHolder

class TextBlocksAdapter (
    private val viewLifecycleOwner: LifecycleOwner,
    private val onClickCallback: (View, String) -> Unit,
) : DataBoundListAdapter<String>(
    diffCallback = object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_text_block,
            parent,
            false)
    }

    override fun bind(binding: ViewDataBinding, item: String, holder: DataBoundViewHolder) {
        when(binding){
            is ListItemTextBlockBinding ->{
                binding.textBlockValue = item
            }
        }
    }


    fun updateTxtBlockList(txtBlockList : List<String>){
        submitList(txtBlockList)
    }
}