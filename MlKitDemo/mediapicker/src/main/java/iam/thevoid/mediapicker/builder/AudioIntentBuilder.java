package iam.thevoid.mediapicker.builder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.core.content.FileProvider;

import java.io.File;

import cafe.adriel.androidaudiorecorder.AudioRecorderActivity;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import iam.thevoid.mediapicker.R;
import iam.thevoid.mediapicker.util.Editor;
import iam.thevoid.mediapicker.util.FileUtil;

import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_AUTO_START;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_CHANNEL;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_COLOR;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_FILE_PATH;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_KEEP_DISPLAY_ON;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_SAMPLE_RATE;
import static cafe.adriel.androidaudiorecorder.AndroidAudioRecorder.EXTRA_SOURCE;

public class AudioIntentBuilder {

    private static final String AUDIO_PATH = "AUDIO_PATH";

    private int flags = 0;

    public AudioIntentBuilder setFlags(int flags) {
        this.flags = flags;
        return this;
    }

    public Intent build(Context context) {
        String filename = Editor.currentDateFilename("", ".wav");

        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), filename);
        if (file.exists()) {
            file.delete();
        }
        int color = context.getResources().getColor(R.color.colorPrimaryDark);

        Uri audioOutput;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String packageName = context.getPackageName();
            audioOutput = FileProvider.getUriForFile(context, packageName + ".fileprovider", file);
        } else {
            audioOutput = Uri.fromFile(file);
        }

        FileUtil.storePhotoPath(context, file.getAbsolutePath());

        return new Intent(context, AudioRecorderActivity.class)
                .putExtra(EXTRA_FILE_PATH, file.getAbsolutePath())
                .putExtra(EXTRA_COLOR, color)
                .putExtra(EXTRA_SOURCE, AudioSource.MIC)
                .putExtra(EXTRA_CHANNEL, AudioChannel.STEREO)
                .putExtra(EXTRA_SAMPLE_RATE, AudioSampleRate.HZ_48000)
                .putExtra(EXTRA_AUTO_START, false)
                .putExtra(EXTRA_KEEP_DISPLAY_ON, true);
    }
}
